describe("GameOfLife", function() {
  var GOL = require('../../src/gameoflife');
  var EXCEPTIONS = require('../../src/exception');

  it('should initialize population properly',function () {
    var population = new GOL(5,5);
    console.log(population.grid);
    expect(population.grid.length).toBeTruthy();
  });

  describe('check if cell live for next gen',function () {
    var population = new GOL(5,5);

    beforeEach(function () {

      population.grid = [
        [ '1', '1', '0', '1', '0' ],
        [ '1', '1', '1', '0', '0' ],
        [ '0', '1', '0', '0', '0' ],
        [ '1', '1', '1', '1', '1' ],
        [ '1', '0', '1', '0', '0' ]
      ];

    })

    it('should report cell alive',function () {
      var flag = population.willCellAlive(0,0);
      expect(flag).toEqual(true);
    });

    it('should report cell dead',function () {
      var flag = population.willCellAlive(0,4);
      expect(flag).toEqual(false);
    });

    it('should raise exception',function () {
      expect(function () {
        population.isCellAlive(4,8);
      }).toThrowError();
    });

  });

  describe('check if next gen grid is as expected',function () {
    var population = new GOL(5,5);

    beforeEach(function () {

      population.grid = [
        [ '1', '1', '0', '1', '0' ],
        [ '1', '1', '1', '1', '0' ],
        [ '0', '0', '0', '0', '0' ],
        [ '1', '1', '1', '1', '1' ],
        [ '1', '0', '1', '1', '0' ]
      ];

    });

    it('next gen population map should be correct',function () {
      population.generateNextGen();
      var expectedGrid = [
        [ '1', '0', '0', '1', '0' ],
        [ '1', '0', '0', '1', '0' ],
        [ '0', '0', '0', '0', '1' ],
        [ '1', '0', '0', '0', '1' ],
        [ '1', '0', '0', '0', '1' ]
      ]
      expect(population.grid).toEqual(expectedGrid);
    });
  });
});

