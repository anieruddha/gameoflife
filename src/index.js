(function () {
  var GOL = require('./gameoflife');
  var obj = new GOL(9,7);
  console.log("Initial Population map");
  console.log(obj.grid);

  setInterval(function() {
    obj.generateNextGen();
    console.log("Next Gen Population map");
    console.log(obj.grid);
  },5000);

})();