/*
Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
Any live cell with two or three live neighbours lives on to the next generation.
Any live cell with more than three live neighbours dies, as if by overpopulation.
Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

* */


function GameOfLife(intRows, intCols) {
    var myexceptions = require('./exception');

    var self = this;
    self.grid = initialize();

    function initialize() {
        var grid = [];
        for (var i = 0; i < intRows; i++) {
            var temp = [];
            for (var j = 0; j < intCols; j++) {
                var num = Math.floor(Math.random() * 2);
                temp[j] = '' + num;
            }
            grid[i] = temp;
        }
        return grid;
    }

    this.willCellAlive = function(rowIdx, colIdx) {

        if (rowIdx < 0 || rowIdx >= intRows) { throw new myexceptions.InvalidIndexPosException() };
        if (colIdx < 0 || colIdx >= intCols) {
            throw new myexceptions.InvalidIndexPosException()
        };

        var neighbours = [];

        if (self.grid[rowIdx - 1]) {
            neighbours.push([rowIdx - 1, colIdx]);

            if (self.grid[rowIdx - 1][colIdx - 1]) {
                neighbours.push([rowIdx - 1, colIdx - 1]);
            }
            if (self.grid[rowIdx - 1][colIdx + 1]) {
                neighbours.push([rowIdx - 1, colIdx + 1]);
            }
        }
        if (self.grid[rowIdx + 1]) {
            neighbours.push([rowIdx + 1, colIdx]);

            if (self.grid[rowIdx + 1][colIdx - 1]) {
                neighbours.push([rowIdx + 1, colIdx - 1]);
            }
            if (self.grid[rowIdx + 1][colIdx + 1]) {
                neighbours.push([rowIdx + 1, colIdx + 1]);
            }
        }
        if (self.grid[rowIdx][colIdx - 1]) {
            neighbours.push([rowIdx, colIdx - 1]);
        }
        if (self.grid[rowIdx][colIdx + 1]) {
            neighbours.push([rowIdx, colIdx + 1]);
        }

        var liveNcount = 0;

        neighbours.forEach(function(pos) {
            liveNcount += parseInt(self.grid[pos[0]][pos[1]]);
        });

        if (liveNcount === 3) return true;
        if (liveNcount === 2 && parseInt(self.grid[rowIdx][colIdx])) return true;

        return false;
    }


    this.generateNextGen = function() {
        var newGrid = [];
        self.grid.forEach(function(row, i) {
            var newRow = [];
            row.forEach(function(cell, j) {
                if (self.willCellAlive(i, j)) newRow[j] = "1";
                else newRow[j] = "0";
            })
            newGrid[i] = newRow;
        });
        self.grid = newGrid;
    }

}

module.exports = GameOfLife;